package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.DateDao
import mx.com.disoftware.mydailytasks.local.LocalDateDataSource
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class LocalDateDataSourceTest {

    private lateinit var database: AppDatabase
    private lateinit var dao: DateDao
    private lateinit var localDataSource: LocalDateDataSource

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
                ApplicationProvider.getApplicationContext(),
                AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.dateDao()
        localDataSource = LocalDateDataSource(dao)
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveDate() = runBlocking {
        val dateExpected = DateEntity(1, "20/09/2021")
        localDataSource.saveDate(dateExpected)
        val dateActual = localDataSource.getAllDate()
        assertThat(dateActual).isNotNull()
        assertThat(dateActual).isNotEmpty()
        assertThat(dateActual[0]).isEqualTo(dateExpected)
    }

    @Test
    fun testDeleteDate() = runBlocking {
        val dateExpected = DateEntity(1, "20/09/2021")
        localDataSource.saveDate(dateExpected)
        localDataSource.deleteDate(dateExpected)
        val dateActual = localDataSource.getAllDate()
        assertThat(dateActual).isEmpty()
        assertThat(dateActual.size).isEqualTo(0)
    }

    @Test
    fun testUpdateDate() = runBlocking {
        val date = DateEntity(1, "20/09/2021")
        localDataSource.saveDate(date)
        val actual = localDataSource.getAllDate()
        actual[0].dateDay = "21/09/2021"
        localDataSource.updateDate(actual[0])
        val dateExpected = DateEntity(1, "21/09/2021")
        assertThat(actual[0]).isEqualTo(dateExpected)
    }

    @Test
    fun testGetAllDate() = runBlocking {
        val dates = ArrayList<DateEntity>(5)
        dates.add(DateEntity(1, "20/09/2021"))
        dates.add(DateEntity(2, "20/09/2021"))
        dates.add(DateEntity(3, "25/08/2021"))
        dates.add(DateEntity(4, "26/08/2021"))
        dates.add(DateEntity(5, "30/12/2021"))
        dates.forEach {
            localDataSource.saveDate(it)
        }
        val expected = localDataSource.getAllDate()
        assertThat(expected).isNotNull()
        assertThat(expected.size).isEqualTo(dates.size)
        assertThat(expected).isEqualTo(dates)
    }

    @Test
    fun testFindByDate() = runBlocking {
        val dateSpecific = "25/10/2021"
        val expected = DateEntity(1, "25/10/2021")
        localDataSource.saveDate(expected)
        val actual = localDataSource.findByDate(dateSpecific)
        assertThat(actual).isNotNull()
        assertThat(actual[0]).isEqualTo(expected)
    }

}