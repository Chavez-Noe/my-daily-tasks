package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.LocalTaskDataSource
import mx.com.disoftware.mydailytasks.local.TaskDao
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class LocalTaskDataSourceTest {

    private lateinit var database: AppDatabase
    private lateinit var dao: TaskDao
    private lateinit var localDataSource: LocalTaskDataSource

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.taskDao()
        localDataSource = LocalTaskDataSource(dao)
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveTask() = runBlocking {
        val expected = TaskEntity(
            1,
            "this title is for testing.",
            "this title is for testing.",
            Status.UNFINISHED.colorExa)
        localDataSource.saveTask(expected)
        val actual = localDataSource.getAllTask()
        assertThat(actual).isNotNull()
        assertThat(actual).isNotEmpty()
        assertThat(actual).contains(expected)
    }

    @Test
    fun testDeleteTask() = runBlocking {
        val task = TaskEntity(
            1,
            "this title is for testing.",
            "this title is for testing.",
            Status.UNFINISHED.colorExa)
        localDataSource.saveTask(task)
        localDataSource.deleteTask(task)
        val tasks = localDataSource.getAllTask()
        assertThat(tasks).isEmpty()
        assertThat(tasks).doesNotContain(task)
    }

    @Test
    fun testGetAllTask() = runBlocking {
        val tasks = ArrayList<TaskEntity>(5)
        var id = 1L
        tasks.forEach {
            it.idTask = id++
            it.condition = "this title is for testing."
            it.description = "this title is for testing."
            it.condition = Status.UNFINISHED.colorExa
            localDataSource.saveTask(it)
        }
        val tasksExpected = localDataSource.getAllTask()
        assertThat(tasksExpected).isNotNull()
        assertThat(tasks).isEqualTo(tasksExpected)
    }

    @Test
    fun testFindTaskByName() = runBlocking {
        val tasks = ArrayList<TaskEntity>(8)
        tasks.add(TaskEntity(1, "prueba 1"))
        tasks.add(TaskEntity(2, "Realizar prueba de repositorio"))
        tasks.add(TaskEntity(3, "pruebas de aceptacion"))
        tasks.add(TaskEntity(4, "diseñar la última prueba"))
        tasks.add(TaskEntity(5, "pruebas y más pruebas"))
        tasks.add(TaskEntity(6, "diseño de base de datos"))
        tasks.add(TaskEntity(7, "junta del servicio social"))
        tasks.add(TaskEntity(8, "lo que nos depare el destino"))
        val palabraBuscada = "prueba"
        tasks.forEach {
            localDataSource.saveTask(it)
        }
        val tasksExpected = localDataSource.findTaskByName(palabraBuscada)
        tasks.remove(TaskEntity(6, "diseño de base de datos"))
        tasks.remove(TaskEntity(7, "junta del servicio social"))
        tasks.remove(TaskEntity(8, "lo que nos depare el destino"))
        assertThat(tasksExpected).isNotEmpty()
        assertThat(tasksExpected).isNotNull()
        assertThat(tasksExpected.size).isEqualTo(tasks.size)
        assertThat(tasksExpected).isEqualTo(tasks)
    }

    @Test
    fun testUpdateTask() = runBlocking {
        val task = TaskEntity(
            1,
            "this title is for testing.",
            "this title is for testing.",
            Status.UNFINISHED.colorExa)
        localDataSource.saveTask(task)
        val taskModify = localDataSource.getAllTask()
        taskModify[0].condition = "Modify"
        taskModify[0].title = "Cambio de titulo"
        taskModify[0].description = "Cambio de descripción"
        localDataSource.updateTask(taskModify[0])
        val taskExpected = localDataSource.getAllTask()
        assertThat(taskModify[0]).isEqualTo(taskExpected[0])
    }

}