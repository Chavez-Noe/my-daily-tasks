package mx.com.disoftware.mydailytasks.domain

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.core.SystemDate
import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.data.local.Status
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.LocalDateDataSource
import mx.com.disoftware.mydailytasks.local.LocalDateTaskCrossRefDataSource
import mx.com.disoftware.mydailytasks.local.LocalTaskDataSource
import mx.com.disoftware.mydailytasks.repository.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class ManageTasksTest {

    private lateinit var dateRepository: DateRepository
    private lateinit var taskRepository: TaskRepository
    private lateinit var dateTaskCrossRefRepository: DateTaskCrossRefRepository
    private lateinit var database: AppDatabase

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dateRepository = DateRepositoryImpl(LocalDateDataSource(database.dateDao()))
        taskRepository = TaskRepositoryImpl(LocalTaskDataSource(database.taskDao()))
        dateTaskCrossRefRepository = DateTaskCrossRefRepositoryImpl(LocalDateTaskCrossRefDataSource(database.dateTaskCrossRefDao()))
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun RegisterNewTaskWithDateAndTaskSuccessfulTest() = runBlocking {
        val task = TaskEntity("Registrarme en la inscripción de Becas", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", Status.UNFINISHED.colorExa)
        val expected = true
        val actual = ManageTasks(dateRepository, taskRepository, dateTaskCrossRefRepository).registerNewTask(task)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun RegisterNewTaskWithExistingDateTest() = runBlocking {
        val task = TaskEntity("Prueba con fecha existente", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", Status.UNFINISHED.colorExa)
        val dateString = SystemDate.getDateSystem()
        val dateExisting = DateEntity(dateString)
        dateRepository.saveDate(dateExisting)
        val expected = true
        val actual = ManageTasks(dateRepository, taskRepository, dateTaskCrossRefRepository).registerNewTask(task)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun RegisterNewTaskWithTaskWithSameTitleAlreadyExistsTest() = runBlocking {
        val titleTask = "Prueba con título de tarea existente"
        val taskExisting = TaskEntity(titleTask, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", Status.UNFINISHED.colorExa)
        taskRepository.save(taskExisting)
        val expected = false
        val actual = ManageTasks(dateRepository, taskRepository, dateTaskCrossRefRepository).registerNewTask(taskExisting)
        assertThat(actual).isEqualTo(expected)
    }

}
