package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.collections.ArrayList

@RunWith(AndroidJUnit4::class)
@SmallTest
class LocalDateTaskCrossRefDataSourceTest {

    private lateinit var database: AppDatabase
    private lateinit var daoCrossRef: DateTaskCrossRefDao
    private lateinit var daoTask: TaskDao
    private lateinit var daoDate: DateDao
    private lateinit var localDateTaskCrossRefDataSource: LocalDateTaskCrossRefDataSource
    private lateinit var localDateDataSource: LocalDateDataSource
    private lateinit var localTaskDataSource: LocalTaskDataSource

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
                ApplicationProvider.getApplicationContext(),
                AppDatabase::class.java
        ).allowMainThreadQueries().build()
        daoCrossRef = database.dateTaskCrossRefDao()
        daoDate = database.dateDao()
        daoTask = database.taskDao()
        localDateTaskCrossRefDataSource = LocalDateTaskCrossRefDataSource(daoCrossRef)
        localDateDataSource = LocalDateDataSource(daoDate)
        localTaskDataSource = LocalTaskDataSource(daoTask)
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveDateTask() = runBlocking {
        val actual = DateTaskCrossRef(1,1)
        localDateTaskCrossRefDataSource.saveDateTask(actual)
        val expected = localDateTaskCrossRefDataSource.getAllDateTask()
        assertThat(expected).isNotNull()
        assertThat(actual).isEqualTo(expected[0])
    }

    @Test
    fun testDeleteDateTask() = runBlocking {
        val actual = DateTaskCrossRef(1,1)
        localDateTaskCrossRefDataSource.saveDateTask(actual)
        localDateTaskCrossRefDataSource.deleteDateTask(actual)
        val expected = localDateTaskCrossRefDataSource.getAllDateTask()
        assertThat(expected).isNotNull()
        assertThat(expected).isEmpty()
        assertThat(expected.size).isEqualTo(0)
    }

    @Test
    fun testGetAllDateTask() = runBlocking {
        val lista = ArrayList<DateTaskCrossRef>(5)
        lista.add(DateTaskCrossRef(1,1))
        lista.add(DateTaskCrossRef(1,2))
        lista.add(DateTaskCrossRef(1,3))
        lista.add(DateTaskCrossRef(2,1))
        lista.add(DateTaskCrossRef(2,2))
        lista.forEach {
            localDateTaskCrossRefDataSource.saveDateTask(it)
        }
        localDateTaskCrossRefDataSource.deleteDateTask(DateTaskCrossRef(2,1))
        localDateTaskCrossRefDataSource.deleteDateTask(DateTaskCrossRef(2,2))
        val listaAlterada = localDateTaskCrossRefDataSource.getAllDateTask()
        assertThat(listaAlterada.size).isEqualTo(lista.size - 2)
    }

    @Test
    fun testTasksBySpecificDate_24_08_2021() = runBlocking {
        val dates = ArrayList<DateEntity>(2)
        val tasks = ArrayList<TaskEntity>(10)
        val lista = ArrayList<DateTaskCrossRef>(14)
        dates.add(DateEntity(1, "24-08-2021"))
        dates.add(DateEntity(2, "25-08-2021"))
        tasks.add(TaskEntity(1, "Task test une", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(2, "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(3, "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...","#008f39"))
        tasks.add(TaskEntity(4, "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(5, "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(6, "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#008f39"))
        tasks.add(TaskEntity(7, "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(8, "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(9, "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(10, "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        lista.add(DateTaskCrossRef(1,1))
        lista.add(DateTaskCrossRef(1,2))
        lista.add(DateTaskCrossRef(1,3))
        lista.add(DateTaskCrossRef(1,4))
        lista.add(DateTaskCrossRef(1,5))
        lista.add(DateTaskCrossRef(2,1))
        lista.add(DateTaskCrossRef(2,2))
        lista.add(DateTaskCrossRef(2,4))
        lista.add(DateTaskCrossRef(2,5))
        lista.add(DateTaskCrossRef(2,6))
        lista.add(DateTaskCrossRef(2,7))
        lista.add(DateTaskCrossRef(2,8))
        lista.add(DateTaskCrossRef(2,9))
        lista.add(DateTaskCrossRef(2,10))
        dates.forEach {
            localDateDataSource.saveDate(it)
        }
        tasks.forEach {
            localTaskDataSource.saveTask(it)
        }
        lista.forEach {
            localDateTaskCrossRefDataSource.saveDateTask(it)
        }
        val actual = localDateTaskCrossRefDataSource.tasksBySpecificDate("24-08-2021")
        val expected = ArrayList<TaskEntity>(5)
        expected.add(TaskEntity(1, "Task test une", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(2, "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        expected.add(TaskEntity(3, "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...","#008f39"))
        expected.add(TaskEntity(4, "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(5, "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        assertThat(actual).isNotNull()
        assertThat(actual.size).isEqualTo(5)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun testTasksBySpecificDate_25_08_2021() = runBlocking {
        val dates = ArrayList<DateEntity>(2)
        val tasks = ArrayList<TaskEntity>(10)
        val lista = ArrayList<DateTaskCrossRef>(14)
        dates.add(DateEntity(1, "24-08-2021"))
        dates.add(DateEntity(2, "25-08-2021"))
        tasks.add(TaskEntity(1, "Task test une", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(2, "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(3, "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...","#008f39"))
        tasks.add(TaskEntity(4, "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(5, "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(6, "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#008f39"))
        tasks.add(TaskEntity(7, "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(8, "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        tasks.add(TaskEntity(9, "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        tasks.add(TaskEntity(10, "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        lista.add(DateTaskCrossRef(1,1))
        lista.add(DateTaskCrossRef(1,2))
        lista.add(DateTaskCrossRef(1,3))
        lista.add(DateTaskCrossRef(1,4))
        lista.add(DateTaskCrossRef(1,5))
        lista.add(DateTaskCrossRef(2,1))
        lista.add(DateTaskCrossRef(2,2))
        lista.add(DateTaskCrossRef(2,4))
        lista.add(DateTaskCrossRef(2,5))
        lista.add(DateTaskCrossRef(2,6))
        lista.add(DateTaskCrossRef(2,7))
        lista.add(DateTaskCrossRef(2,8))
        lista.add(DateTaskCrossRef(2,9))
        lista.add(DateTaskCrossRef(2,10))
        dates.forEach {
            localDateDataSource.saveDate(it)
        }
        tasks.forEach {
            localTaskDataSource.saveTask(it)
        }
        lista.forEach {
            localDateTaskCrossRefDataSource.saveDateTask(it)
        }
        val actual = localDateTaskCrossRefDataSource.tasksBySpecificDate("25-08-2021")
        val expected = ArrayList<TaskEntity>(5)
        expected.add(TaskEntity(1, "Task test une", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(2, "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        expected.add(TaskEntity(4, "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(5, "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(6, "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#008f39"))
        expected.add(TaskEntity(7, "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        expected.add(TaskEntity(8, "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff8000"))
        expected.add(TaskEntity(9, "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        expected.add(TaskEntity(10, "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...", "#ff0000"))
        assertThat(actual).isNotNull()
        assertThat(actual.size).isEqualTo(9)
        assertThat(actual).isEqualTo(expected)
    }

}