package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.DateTaskCrossRefDao
import com.google.common.truth.Truth.assertThat
import mx.com.disoftware.mydailytasks.local.DateDao
import mx.com.disoftware.mydailytasks.local.TaskDao
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class DateTaskCrossRefDaoTest {

    private lateinit var database: AppDatabase
    private lateinit var dao: DateTaskCrossRefDao
    private lateinit var daoTask: TaskDao
    private lateinit var daoDate: DateDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.dateTaskCrossRefDao()
        daoTask = database.taskDao()
        daoDate = database.dateDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveDateTask() = runBlocking {
        val actual = DateTaskCrossRef(1,1)
        dao.saveDateTask(actual)
        val expected = dao.getAllDateTask()
        assertThat(expected).isNotNull()
        assertThat(actual).isEqualTo(expected[0])
    }

    @Test
    fun testDeleteDateTask() = runBlocking {
        val actual = DateTaskCrossRef(1, 1)
        dao.saveDateTask(actual)
        dao.deleteDateTask(actual)
        val expected = dao.getAllDateTask()
        assertThat(expected).isEmpty()
    }

    @Test
    fun testTasksBySpecificDateWith24_08_2021() = runBlocking {
        val tasks = arrayListOf<TaskEntity>()
        tasks.add(
            TaskEntity(
                1,
                "Task test one", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                2,
                "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                3,
                "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                4,
                "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                5,
                "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                6,
                "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                7,
                "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                8,
                "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                9,
                "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                10,
                "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.forEach {
            daoTask.saveTask(it)
        }
        val dates = arrayListOf<DateEntity>()
        dates.add(
            DateEntity(1, "24-08-2021")
        )
        dates.add(
            DateEntity(2, "25-08-2021")
        )
        dates.forEach {
            daoDate.saveDate(it)
        }
        val lista = arrayListOf<DateTaskCrossRef>()
        lista.add(DateTaskCrossRef(1,1 ))
        lista.add(DateTaskCrossRef(1,2 ))
        lista.add(DateTaskCrossRef(1,3 ))
        lista.add(DateTaskCrossRef(1,4 ))
        lista.add(DateTaskCrossRef(1,5 ))
        lista.add(DateTaskCrossRef(2,1 ))
        lista.add(DateTaskCrossRef(2,2 ))
        lista.add(DateTaskCrossRef(2,4 ))
        lista.add(DateTaskCrossRef(2,5 ))
        lista.add(DateTaskCrossRef(2,6 ))
        lista.add(DateTaskCrossRef(2,7 ))
        lista.add(DateTaskCrossRef(2,8 ))
        lista.add(DateTaskCrossRef(2,9 ))
        lista.add(DateTaskCrossRef(2,10 ))
        lista.forEach {
            dao.saveDateTask(it)
        }
        val expected = arrayListOf<TaskEntity>()
        expected.add(
            TaskEntity(
                1,
                "Task test one", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                2,
                "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        expected.add(
            TaskEntity(
                3,
                "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                4,
                "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                5,
                "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        val actual = dao.tasksBySpecificDate("24-08-2021")
        assertThat(actual).isNotNull()
        assertThat(actual).isNotEmpty()
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun testTasksBySpecificDateWith25_08_2021() = runBlocking {
        val tasks = arrayListOf<TaskEntity>()
        tasks.add(
            TaskEntity(
                1,
                "Task test one", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                2,
                "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                3,
                "Task test three", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                4,
                "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                5,
                "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                6,
                "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                7,
                "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                8,
                "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        tasks.add(
            TaskEntity(
                9,
                "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.add(
            TaskEntity(
                10,
                "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        tasks.forEach {
            daoTask.saveTask(it)
        }
        val dates = arrayListOf<DateEntity>()
        dates.add(
            DateEntity(1, "24-08-2021")
        )
        dates.add(
            DateEntity(2, "25-08-2021")
        )
        dates.forEach {
            daoDate.saveDate(it)
        }
        val lista = arrayListOf<DateTaskCrossRef>()
        lista.add(DateTaskCrossRef(1,1 ))
        lista.add(DateTaskCrossRef(1,2 ))
        lista.add(DateTaskCrossRef(1,3 ))
        lista.add(DateTaskCrossRef(1,4 ))
        lista.add(DateTaskCrossRef(1,5 ))
        lista.add(DateTaskCrossRef(2,1 ))
        lista.add(DateTaskCrossRef(2,2 ))
        lista.add(DateTaskCrossRef(2,4 ))
        lista.add(DateTaskCrossRef(2,5 ))
        lista.add(DateTaskCrossRef(2,6 ))
        lista.add(DateTaskCrossRef(2,7 ))
        lista.add(DateTaskCrossRef(2,8 ))
        lista.add(DateTaskCrossRef(2,9 ))
        lista.add(DateTaskCrossRef(2,10 ))
        lista.forEach {
            dao.saveDateTask(it)
        }
        val expected = arrayListOf<TaskEntity>()
        expected.add(
            TaskEntity(
                1,
                "Task test one", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                2,
                "Task test two", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        expected.add(
            TaskEntity(
                4,
                "Task test four", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                5,
                "Task test five", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                6,
                "Task test six", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.FINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                7,
                "Task test seven", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        expected.add(
            TaskEntity(
                8,
                "Task test eight", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.PARTIALLY.colorExa)
        )
        expected.add(
            TaskEntity(
                9,
                "Task test nine", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        expected.add(
            TaskEntity(
                10,
                "Task test ten", "This is a test task, so it is being tested if it is saved correctly, for queries ...",
                Status.UNFINISHED.colorExa)
        )
        val actual = dao.tasksBySpecificDate("25-08-2021")
        assertThat(actual).isNotNull()
        assertThat(actual).isNotEmpty()
        assertThat(actual).isEqualTo(expected)
    }

}