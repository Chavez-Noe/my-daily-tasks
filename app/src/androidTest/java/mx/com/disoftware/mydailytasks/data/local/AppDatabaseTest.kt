package mx.com.disoftware.mydailytasks.data.local

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import mx.com.disoftware.mydailytasks.local.AppDatabase
import org.junit.Test
import org.junit.runner.RunWith
import com.google.common.truth.Truth.assertThat
import com.google.errorprone.annotations.CompatibleWith
import mx.com.disoftware.mydailytasks.local.DateDao
import mx.com.disoftware.mydailytasks.local.DateTaskCrossRefDao
import mx.com.disoftware.mydailytasks.local.TaskDao
import org.junit.After
import org.junit.Before

@RunWith(AndroidJUnit4::class)
@SmallTest
class AppDatabaseTest {

    private lateinit var instaceDB: AppDatabase

    @Before
    fun setup() {
        instaceDB = AppDatabase
            .getDatabase(ApplicationProvider.getApplicationContext())
    }

    @After
    fun teardown() {
        instaceDB.close()
    }

    @Test
    fun testGetDatabase() {
        assertThat(instaceDB).isNotNull()
        assertThat(instaceDB).isInstanceOf(AppDatabase::class.java)
    }

    @Test
    fun testInstanceOfTaskDao() {
        val taskDao = instaceDB.taskDao()
        assertThat(taskDao).isNotNull()
        assertThat(taskDao).isInstanceOf(TaskDao::class.java)
    }

    @Test
    fun testInstanceOfDateDao() {
        val dateDao = instaceDB.dateDao()
        assertThat(dateDao).isNotNull()
        assertThat(dateDao).isInstanceOf(DateDao::class.java)
    }

    @Test
    fun testInstanceOfDateTaskCrossRefDao() {
        val dateTaskCrossRefDao = instaceDB.dateTaskCrossRefDao()
        assertThat(dateTaskCrossRefDao).isNotNull()
        assertThat(dateTaskCrossRefDao)
            .isInstanceOf(DateTaskCrossRefDao::class.java)
    }

}