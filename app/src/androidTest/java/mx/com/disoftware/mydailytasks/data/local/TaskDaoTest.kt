package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.TaskDao
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class TaskDaoTest {

    private lateinit var database: AppDatabase
    private lateinit var dao: TaskDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.taskDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveTask() = runBlocking {
        val task = TaskEntity(
            1,
            "Tarea de prueba",
            "tarea de prueba en memoria par los tests",
            Status.UNFINISHED.colorExa)
        dao.saveTask(task)
        val allTask = dao.getAllTasks()
        assertThat(allTask).contains(task)
    }

    @Test
    fun testDeleteTask() = runBlocking {
        val task = TaskEntity(
            1,
            "Tarea de prueba",
            "tarea de prueba en memoria par los tests",
            Status.UNFINISHED.colorExa)
        dao.saveTask(task)
        dao.deleteTask(task)
        val tasks = dao.getAllTasks()
        assertThat(tasks).doesNotContain(task)
    }

    @Test
    fun testFindTaskByNameSpecifically() = runBlocking {
        val task = TaskEntity(
            1,
            "Tests TaskDao",
            "Realizando Test para el Dao de tareas.",
            Status.FINISHED.colorExa)
        dao.saveTask(task)
        val listTask = dao.findTaskByName("Tests TaskDao")
        assertThat(listTask).contains(task)

    }

    @Test
    fun testFindTaskByKeyWord() = runBlocking {
        val keyword = "compra"
        val taskCurrent = ArrayList<TaskEntity>(6)
        taskCurrent.add(TaskEntity(1, "compras en el Soriana", "preuba", Status.UNFINISHED.colorExa))
        taskCurrent.add(TaskEntity(2, "Ir de compras al tianguis", "preuba", Status.UNFINISHED.colorExa))
        taskCurrent.add(TaskEntity(3, "comprar medicinas", "preuba", Status.UNFINISHED.colorExa))
        taskCurrent.add(TaskEntity(4, "Lista de compras especial", "preuba", Status.UNFINISHED.colorExa))
        taskCurrent.add(TaskEntity(5, "compras navideñas", "preuba", Status.UNFINISHED.colorExa))
        taskCurrent.add(TaskEntity(6, "Pagar el mantenimiento", "preuba", Status.UNFINISHED.colorExa))

        taskCurrent.forEach {
            dao.saveTask(it)
        }
        val tasksExpected = dao.findTaskByName(keyword)
        assertThat(tasksExpected).hasSize(taskCurrent.size - 1)
        taskCurrent.removeAt(5)
        assertThat(tasksExpected).isEqualTo(taskCurrent)
    }

    @Test
    fun testUpdateTaskToFinished() = runBlocking {
        val taskFinished = TaskEntity(
            1,
            "Tests TaskDao",
            "Realizando Test para el Dao de tareas.",
            Status.UNFINISHED.colorExa)
        dao.saveTask(taskFinished)
        taskFinished.condition = Status.FINISHED.colorExa
        dao.updateTask(taskFinished)
        val taskUpdate = dao.findTaskByName("Tests TaskDao")
        assertThat(taskUpdate).isNotNull()
        assertThat(taskFinished.condition).isEqualTo(taskUpdate[0].condition)
    }

    @Test
    fun testUpdateTaskToPartially() = runBlocking {
        val taskFinished = TaskEntity(
            1,
            "Tests TaskDao",
            "Realizando Test para el Dao de tareas.",
            Status.UNFINISHED.colorExa)
        dao.saveTask(taskFinished)
        taskFinished.condition = Status.PARTIALLY.colorExa
        dao.updateTask(taskFinished)
        val taskUpdate = dao.findTaskByName("Tests TaskDao")
        assertThat(taskUpdate).isNotNull()
        assertThat(taskFinished.condition).isEqualTo(taskUpdate[0].condition)
    }

    @Test
    fun testUpdateTaskToUnfinished() = runBlocking {
        val taskFinished = TaskEntity(
            1,
            "Tests TaskDao",
            "Realizando Test para el Dao de tareas.",
            Status.FINISHED.colorExa)
        dao.saveTask(taskFinished)
        taskFinished.condition = Status.UNFINISHED.colorExa
        dao.updateTask(taskFinished)
        val taskUpdate = dao.findTaskByName("Tests TaskDao")
        assertThat(taskUpdate).isNotNull()
        assertThat(taskFinished.condition).isEqualTo(taskUpdate[0].condition)
    }

}