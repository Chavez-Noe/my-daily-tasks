package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.DateDao
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class DateDaoTest {

    private lateinit var database: AppDatabase
    private lateinit var dao: DateDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.dateDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun testSaveDate() = runBlocking {
        val date = DateEntity(1, "08/09/2021")
        dao.saveDate(date)
        val allDate = dao.getAllDate()
        assertThat(allDate).contains(date)
    }

    @Test
    fun testDeleteDate() = runBlocking {
        val date = DateEntity(1, "08/09/2021")
        dao.saveDate(date)
        dao.deleteDate(date)
        val dates = dao.getAllDate()
        assertThat(dates).doesNotContain(date)
    }

    @Test
    fun testUpdateDate() = runBlocking {
        val date = DateEntity(1, "08/09/2021")
        dao.saveDate(date)
        date.dateDay = "09/09/2021"
        dao.updateDate(date)
        assertThat(date.dateDay).isEqualTo("09/09/2021")
    }

    @Test
    fun testGetAllDate() = runBlocking {
        val dates = ArrayList<DateEntity>(5)
        dates.add(DateEntity(1, "08/09/2021"))
        dates.add(DateEntity(2, "09/09/2021"))
        dates.add(DateEntity(3, "10/09/2021"))
        dates.add(DateEntity(4, "11/09/2021"))
        dates.add(DateEntity(5, "12/09/2021"))
        dates.forEach {
            dao.saveDate(it)
        }
        val datesExpected = dao.getAllDate()
        assertThat(datesExpected).isNotNull()
        assertThat(datesExpected).isEqualTo(dates)
    }

    @Test
    fun testFindByDateSpecify() = runBlocking {
        val dates = ArrayList<DateEntity>(5)
        dates.add(DateEntity(1, "08/09/2021"))
        dates.add(DateEntity(2, "09/09/2021"))
        dates.add(DateEntity(3, "10/09/2021"))
        dates.add(DateEntity(4, "11/09/2021"))
        dates.add(DateEntity(5, "12/09/2021"))
        dates.forEach {
            dao.saveDate(it)
        }
        val keyword = "08/09/2021"
        val dateExpected = dao.findByDate(keyword)
        assertThat(dateExpected).isNotNull()
        assertThat(dateExpected[0]).isEqualTo(dates[0])
    }

    @Test
    fun testFindByDateKeyword() = runBlocking {
        val dates = ArrayList<DateEntity>(5)
        dates.add(DateEntity(1, "08/09/2021"))
        dates.add(DateEntity(2, "09/09/2021"))
        dates.add(DateEntity(3, "10/09/2021"))
        dates.add(DateEntity(4, "11/12/2018"))
        dates.add(DateEntity(5, "12/08/2019"))
        dates.forEach {
            dao.saveDate(it)
        }
        dates.remove(DateEntity(4, "11/12/2018"))
        dates.remove(DateEntity(5, "12/08/2019"))
        val keyword = "/09/202"
        val dateExpected = dao.findByDate(keyword)
        assertThat(dateExpected).isNotNull()
        assertThat(dateExpected).hasSize(dates.size)
        assertThat(dateExpected).isEqualTo(dates)
    }

}