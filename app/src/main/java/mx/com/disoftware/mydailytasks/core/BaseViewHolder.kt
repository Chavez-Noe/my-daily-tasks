package mx.com.disoftware.mydailytasks.core

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * le podemos pasar cualquier ViewHolder y nos regresa el item que queremos acceder.
 */
abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T)
}