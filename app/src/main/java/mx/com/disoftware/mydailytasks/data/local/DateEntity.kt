package mx.com.disoftware.mydailytasks.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DateEntity (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_date")
    val idDate: Long = 1L,
    @ColumnInfo(name = "date_day")
    var dateDay: String = "Current Day"
) {
    constructor(dateDay: String) : this (0, dateDay)
}