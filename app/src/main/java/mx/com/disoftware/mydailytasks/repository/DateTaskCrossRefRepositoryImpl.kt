package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.local.LocalDateTaskCrossRefDataSource

class DateTaskCrossRefRepositoryImpl(
        private val localDateTaskCrossRefDataSource: LocalDateTaskCrossRefDataSource
        ) : DateTaskCrossRefRepository {

    override suspend fun saveDateTask(dateTaskCrossRef: DateTaskCrossRef) = localDateTaskCrossRefDataSource.saveDateTask(dateTaskCrossRef)

    override suspend fun deleteDateTask(dateTaskCrossRef: DateTaskCrossRef) = localDateTaskCrossRefDataSource.deleteDateTask(dateTaskCrossRef)

    override suspend fun getDateIdsAndTaskIds() = localDateTaskCrossRefDataSource.getAllDateTask()

    override suspend fun tasksBySpecificDate(date: String) = localDateTaskCrossRefDataSource.tasksBySpecificDate(date)

}