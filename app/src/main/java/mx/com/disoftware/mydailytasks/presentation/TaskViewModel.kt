    package mx.com.disoftware.mydailytasks.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.repository.TaskRepository
import java.lang.Exception

class TaskViewModel(private val repo: TaskRepository) : ViewModel() {

    fun fetchTask() = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.getTasks()))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }

    }

    fun saveTask(taskEntity: TaskEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.save(taskEntity)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun findTaskByName(word: String) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.findTaskByName(word)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun updateTask(taskEntity: TaskEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.updateTask(taskEntity)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun deleteTask(taskEntity: TaskEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.deleteTask(taskEntity)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

}

class TaskViewModelFactory(private val repo: TaskRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(TaskRepository::class.java).newInstance(repo)
    }

}


