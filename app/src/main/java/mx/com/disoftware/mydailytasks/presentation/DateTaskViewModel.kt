package mx.com.disoftware.mydailytasks.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.repository.DateTaskCrossRefRepository

class DateTaskViewModel(private val repo: DateTaskCrossRefRepository) : ViewModel() {

    fun saveDateTask(dateTask: DateTaskCrossRef) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.saveDateTask(dateTask)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun deleteDateTask(dateTask: DateTaskCrossRef) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.deleteDateTask(dateTask)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun getDateIdsAndTaskIds() = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.getDateIdsAndTaskIds()))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun tasksBySpecificDate(date: String) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.tasksBySpecificDate(date)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

}

class DateTaskViewModelFactory(private val repo: DateTaskCrossRefRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(DateTaskCrossRefRepository::class.java).newInstance(repo)
    }

}