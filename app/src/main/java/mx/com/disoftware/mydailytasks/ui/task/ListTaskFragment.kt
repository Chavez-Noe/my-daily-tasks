package mx.com.disoftware.mydailytasks.ui.task

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import mx.com.disoftware.mydailytasks.R
import mx.com.disoftware.mydailytasks.core.SystemDate
import mx.com.disoftware.mydailytasks.databinding.FragmentListTaskBinding
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.LocalDateTaskCrossRefDataSource
import mx.com.disoftware.mydailytasks.presentation.DateTaskViewModel
import mx.com.disoftware.mydailytasks.presentation.DateTaskViewModelFactory
import mx.com.disoftware.mydailytasks.repository.DateTaskCrossRefRepositoryImpl
import mx.com.disoftware.mydailytasks.ui.task.adapter.ListTaskAdapter
import mx.com.disoftware.mydailytasks.core.*
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

class ListTaskFragment : Fragment(R.layout.fragment_list_task) {

    private lateinit var fragmentComponents: FragmentListTaskBinding
    private var listTask: List<TaskEntity> = listOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentComponents = FragmentListTaskBinding.bind(view)

        val dataLocal = LocalDateTaskCrossRefDataSource(AppDatabase.getDatabase(requireContext())
            .dateTaskCrossRefDao())

        val viewModel by viewModels<DateTaskViewModel> {
            DateTaskViewModelFactory(DateTaskCrossRefRepositoryImpl(dataLocal))
        }

        viewModel.tasksBySpecificDate(SystemDate.getDateSystem()).observe(viewLifecycleOwner, { answer ->
            when (answer) {
                is Result.Loading -> {
                    fragmentComponents.progressBarListTask.visibility = View.VISIBLE
                    Log.d("LiveData", "Buscando con la palabra clave ${SystemDate.getDateSystem()}")
                }
                is Result.Success -> {
                    Log.d("LiveData", "${answer.data}")
                    listTask = answer.data
                    if(answer.data.isNotEmpty()){
                        fragmentComponents.lottieAnimationViewBackgroundAnimationEmpty.visibility = View.GONE
                    }
                    fragmentComponents.recyclerViewContent.adapter = ListTaskAdapter(listTask)
                    fragmentComponents.recyclerViewContent.visibility = View.VISIBLE
                    fragmentComponents.progressBarListTask.visibility = View.GONE

                }
                is Result.Failure -> Log.d("Error", "${answer.exception}")
            }
        })

        fragmentComponents.fabAddTask.setOnClickListener {
            addTask()
        }

    }

    private fun addTask() {
        findNavController().navigate(R.id.action_listTaskFragment_to_addTaskFragment)
    }

}