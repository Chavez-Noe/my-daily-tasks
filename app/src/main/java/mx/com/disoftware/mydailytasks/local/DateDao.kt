package mx.com.disoftware.mydailytasks.local

import androidx.room.*
import mx.com.disoftware.mydailytasks.data.local.DateEntity

@Dao
interface DateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveDate(date: DateEntity)

    @Delete
    suspend fun deleteDate(date: DateEntity)

    @Update
    suspend fun updateDate(date: DateEntity)

    @Query("SELECT * FROM dateentity")
    suspend fun getAllDate(): List<DateEntity>

    @Query("SELECT * FROM dateentity WHERE date_day LIKE '%' || :nameDate || '%'")
    suspend fun findByDate(nameDate: String): List<DateEntity>

}