package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.local.LocalDateDataSource

class DateRepositoryImpl(private val dataSourceLocal: LocalDateDataSource) : DateRepository {

    override suspend fun saveDate(date: DateEntity) = dataSourceLocal.saveDate(date)

    override suspend fun deleteDate(date: DateEntity) = dataSourceLocal.deleteDate(date)

    override suspend fun updateDate(date: DateEntity) = dataSourceLocal.updateDate(date)

    override suspend fun getDates() = dataSourceLocal.getAllDate()

    override suspend fun findByDate(nameDate: String) = dataSourceLocal.findByDate(nameDate)

}