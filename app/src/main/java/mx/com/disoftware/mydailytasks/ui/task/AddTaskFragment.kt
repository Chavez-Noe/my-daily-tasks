package mx.com.disoftware.mydailytasks.ui.task

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.android.awaitFrame
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.R
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.data.local.Status
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.databinding.FragmentAddTaskBinding
import mx.com.disoftware.mydailytasks.domain.ManageTasks
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.LocalDateDataSource
import mx.com.disoftware.mydailytasks.local.LocalDateTaskCrossRefDataSource
import mx.com.disoftware.mydailytasks.local.LocalTaskDataSource
import mx.com.disoftware.mydailytasks.presentation.ManagerTaskViewModel
import mx.com.disoftware.mydailytasks.presentation.ManagerTaskViewModelFactory
import mx.com.disoftware.mydailytasks.repository.*

// para hacer un BottomSheetDialogFragment -> AddTaskFragment : BottomSheetDialogFragment()
class AddTaskFragment : Fragment(R.layout.fragment_add_task) {

    private lateinit var fragmentComponents: FragmentAddTaskBinding
    private lateinit var dateLocal: LocalDateDataSource
    private lateinit var taskLocal: LocalTaskDataSource
    private lateinit var dateTaskLocal: LocalDateTaskCrossRefDataSource
    private lateinit var dateRepository: DateRepository
    private lateinit var taskRepository: TaskRepository
    private lateinit var crossRefRepository: DateTaskCrossRefRepository

    private var title = ""
    private var description = ""
    private var status = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dateLocal = LocalDateDataSource(AppDatabase.getDatabase(requireContext()).dateDao())
        taskLocal = LocalTaskDataSource(AppDatabase.getDatabase(requireContext()).taskDao())
        dateTaskLocal = LocalDateTaskCrossRefDataSource(AppDatabase.getDatabase(requireContext())
            .dateTaskCrossRefDao())

        dateRepository = DateRepositoryImpl(dateLocal)
        taskRepository = TaskRepositoryImpl(taskLocal)
        crossRefRepository = DateTaskCrossRefRepositoryImpl(dateTaskLocal)

        fragmentComponents = FragmentAddTaskBinding.bind(view)

        fragmentComponents.materialButtonSaveTask.setOnClickListener {
            collectDataFromNewTask()
        }

    }

    private fun collectDataFromNewTask() {

        title = fragmentComponents.textInputEditTextTitleTask.text.toString()
        description = fragmentComponents.textInputEditTextDescriptionTask.text.toString()
        status = Status.UNFINISHED.colorExa

        if (validateFields()) {

            val task = TaskEntity(title, description, status)

            val viewModel by viewModels<ManagerTaskViewModel> {
                ManagerTaskViewModelFactory(ManageTasks(dateRepository, taskRepository, crossRefRepository))
            }

           viewModel.registerNewTask(task).observe(this, { answer ->
                when(answer) {
                    is Result.Loading -> {
                        fragmentComponents.lottieAnimationViewSuccessfully.visibility = View.VISIBLE
                        fragmentComponents.lottieAnimationViewSuccessfully.progress
                        Log.d("Add_Task", "Cargando...")
                    }
                    is Result.Success -> {
                        fragmentComponents.lottieAnimationViewSuccessfully.playAnimation()
                        fragmentComponents.textInputEditTextTitleTask.setText("")
                        fragmentComponents.textInputEditTextDescriptionTask.setText("")
                        Log.d("Add_Task", "Tarea ${task} guadada con éxito")
                    }
                    is Result.Failure -> {
                        fragmentComponents.lottieAnimationViewSuccessfully.cancelAnimation()
                        fragmentComponents.lottieAnimationViewSuccessfully.visibility = View.GONE
                        Log.d("Add_Task", "Error al guardar")
                    }
                }
            })

        }

    }

    private fun validateFields(): Boolean {

        var isValid = true

        if (title.isEmpty())
        {
            fragmentComponents.textInputLayoutTitle.error = getString(R.string.error_text_input_empty_title)
            fragmentComponents.textInputLayoutTitle.requestFocus()
            isValid = false
        } else {
            fragmentComponents.textInputLayoutTitle.error = null
        }

        if (description.isEmpty())
        {
            fragmentComponents.textInputLayoutDesciption.error = getString(R.string.error_text_input_empty_description)
            fragmentComponents.textInputLayoutTitle.requestFocus()
            isValid = false
        } else {
            fragmentComponents.textInputLayoutDesciption.error = null
        }

        return isValid
    }

    companion object {
        const val TAG = "Agregar Nueva Tarea"
    }

}