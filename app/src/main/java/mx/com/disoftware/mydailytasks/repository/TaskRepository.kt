package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.data.local.TaskEntity

interface TaskRepository {

    suspend fun getTasks(): List<TaskEntity>

    suspend fun save(task: TaskEntity)

    suspend fun findTaskByName(nameTask: String): List<TaskEntity>

    suspend fun updateTask(task: TaskEntity)

    suspend fun deleteTask(task: TaskEntity)

}