package mx.com.disoftware.mydailytasks.local

import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef

class LocalDateTaskCrossRefDataSource(private val dateTaskCrossRefDao: DateTaskCrossRefDao) {

    suspend fun saveDateTask(dateTaskCrossRef: DateTaskCrossRef) {
        dateTaskCrossRefDao.saveDateTask(dateTaskCrossRef)
    }

    suspend fun deleteDateTask(dateTaskCrossRef: DateTaskCrossRef) {
        dateTaskCrossRefDao.deleteDateTask(dateTaskCrossRef)
    }

    suspend fun getAllDateTask() = dateTaskCrossRefDao.getAllDateTask()

    suspend fun tasksBySpecificDate(date: String) = dateTaskCrossRefDao.tasksBySpecificDate(date)

}