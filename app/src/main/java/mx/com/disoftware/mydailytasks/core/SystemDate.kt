package mx.com.disoftware.mydailytasks.core

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class SystemDate {

    companion object {

        private var dateSystem = Date()
        private var format: SimpleDateFormat? = null
        private const val FORMAT_DATE_STRING = "dd/MM/yyyy"
        private const val FORMAT_HOURS_STRING = "HH:mm"

        fun getDateSystem(): String {
            format = SimpleDateFormat(FORMAT_DATE_STRING)
            return format!!.format(dateSystem)
        }

        fun getCurrentTime(): String {
            format = SimpleDateFormat(FORMAT_HOURS_STRING)
            return format!!.format(dateSystem)
        }

        fun getPreviousDate(): String {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val actualDate = LocalDate.now()
                var yesterday = actualDate.minusDays(1)
                val format = DateTimeFormatter.ofPattern(FORMAT_DATE_STRING)
                return yesterday.format(format)
            }

            return "Build.VERSION.SDK_INT"
        }

    }

}