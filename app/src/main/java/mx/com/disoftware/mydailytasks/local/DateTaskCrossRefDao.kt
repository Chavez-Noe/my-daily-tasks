package mx.com.disoftware.mydailytasks.local

import androidx.room.*
import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

@Dao
interface DateTaskCrossRefDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveDateTask(dateTaskCrossRef: DateTaskCrossRef)

    @Delete
    suspend fun deleteDateTask(dateTaskCrossRef: DateTaskCrossRef)

    @Query("SELECT * FROM datetaskcrossref")
    suspend fun getAllDateTask(): List<DateTaskCrossRef>

   @Query(
        "SELECT * " +
              "FROM taskentity " +
              "INNER JOIN datetaskcrossref ON taskentity.id_task = datetaskcrossref.idTask " +
              "INNER JOIN dateentity ON dateentity.id_date = datetaskcrossref.idDate " +
              "WHERE dateentity.date_day = :date"
    )
    suspend fun tasksBySpecificDate(date: String): List<TaskEntity>

}