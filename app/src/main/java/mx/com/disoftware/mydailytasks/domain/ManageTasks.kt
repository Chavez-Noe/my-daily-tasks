package mx.com.disoftware.mydailytasks.domain

import androidx.lifecycle.ViewModel
import mx.com.disoftware.mydailytasks.core.SystemDate
import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.repository.DateRepository
import mx.com.disoftware.mydailytasks.repository.DateTaskCrossRefRepository
import mx.com.disoftware.mydailytasks.repository.TaskRepository

class ManageTasks(
    private val dateRepository: DateRepository,
    private val taskRepository: TaskRepository,
    private val dateTaskCrossRefRepository: DateTaskCrossRefRepository
) {

    /**
     * Devuelve verdadero en caso de tener éxito y falso en caso contrario.
     */
    suspend fun registerNewTask(taskEntity: TaskEntity): Boolean {

        var date: DateEntity? = null

        // Validar si existe una tarea con un mismo título.
        var taskRepo = taskRepository.findTaskByName(taskEntity.title)

        if (!taskRepo.isNullOrEmpty())
            return false
        // Guardamos la tarea después de comprobar que no existe una misma tarea con el mismo nombre.
        taskRepository.save(taskEntity)
        // Validamos si en la DB existe la fecha actual (puede que ya se hallan agregado una tarea este mismo día).
        val dateString = SystemDate.getDateSystem()
        var dateRepo = dateRepository.findByDate(dateString)
        if (dateRepo.isNullOrEmpty()) {
            // Creamos la fecha con la fecha actual obtenida del sistema operativo.
            date = DateEntity(dateString)
            dateRepository.saveDate(date)
        }

        // Obtenemos el id de la tarea que se acaba de guardar y de la fecha actual.
        taskRepo = taskRepository.findTaskByName(taskEntity.title)
        dateRepo = dateRepository.findByDate(dateString)
        val dateId = dateRepo[0].idDate
        val taskId = taskRepo[0].idTask
        // Vinculamos la fecha actual con la nueva tarea creada.
        dateTaskCrossRefRepository.saveDateTask(DateTaskCrossRef(dateId, taskId))

        return true

    }

}