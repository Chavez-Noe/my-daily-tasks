package mx.com.disoftware.mydailytasks.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

@Database(
    entities = [TaskEntity::class, DateEntity::class, DateTaskCrossRef::class],
    version = 2
)
abstract class AppDatabase() : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun dateDao(): DateDao
    abstract fun dateTaskCrossRefDao(): DateTaskCrossRefDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            INSTANCE = INSTANCE ?: Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "Tasks_App_DB" // DB name.
            ).build()
            return INSTANCE!!
        }

    }

}