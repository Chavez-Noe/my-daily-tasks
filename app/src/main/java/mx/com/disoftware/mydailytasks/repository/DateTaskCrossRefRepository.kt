package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.data.local.DateTaskCrossRef
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

interface DateTaskCrossRefRepository {

    suspend fun saveDateTask(dateTaskCrossRef: DateTaskCrossRef)

    suspend fun deleteDateTask(dateTaskCrossRef: DateTaskCrossRef)

    suspend fun getDateIdsAndTaskIds(): List<DateTaskCrossRef>

    suspend fun tasksBySpecificDate(date: String): List<TaskEntity>

}