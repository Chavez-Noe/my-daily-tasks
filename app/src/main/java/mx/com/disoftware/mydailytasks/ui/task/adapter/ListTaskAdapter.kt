package mx.com.disoftware.mydailytasks.ui.task.adapter

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.recyclerview.widget.RecyclerView
import mx.com.disoftware.mydailytasks.R
import mx.com.disoftware.mydailytasks.data.local.Status
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

class ListTaskAdapter(
    private val listTasks: List<TaskEntity>
) : RecyclerView.Adapter<ListTaskAdapter.HomeScreenViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeScreenViewHolder {
        val itemTask = LayoutInflater.from(parent.context)
            .inflate(R.layout.task_item, parent, false)
        return HomeScreenViewHolder(itemTask)
    }

    override fun onBindViewHolder(holder: HomeScreenViewHolder, position: Int) {
        val taskEntity: TaskEntity = listTasks[position]
        holder.taskItemTitle.text = taskEntity.title
        holder.taskItemDescription.text = taskEntity.description
        when(taskEntity.condition){
            Status.UNFINISHED.colorExa -> holder.viewDivColorState.setBackgroundResource(R.color.unfinished)
            Status.PARTIALLY.colorExa -> holder.viewDivColorState.setBackgroundResource(R.color.partially)
            Status.FINISHED.colorExa -> holder.viewDivColorState.setBackgroundResource(R.color.finished)
        }
    }

    override fun getItemCount() = listTasks.size

    inner class HomeScreenViewHolder(
        private val itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        val taskItemTitle: TextView = itemView.findViewById(R.id.task_item_title)
        val taskItemDescription: TextView = itemView.findViewById(R.id.task_item_description)
        val viewDivColorState: View = itemView.findViewById(R.id.view_div_color_state)
    }

}