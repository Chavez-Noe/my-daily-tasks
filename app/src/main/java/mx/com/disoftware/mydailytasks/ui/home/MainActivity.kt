package mx.com.disoftware.mydailytasks.ui.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import mx.com.disoftware.mydailytasks.R
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.core.SystemDate
import mx.com.disoftware.mydailytasks.local.AppDatabase
import mx.com.disoftware.mydailytasks.local.LocalDateTaskCrossRefDataSource
import mx.com.disoftware.mydailytasks.presentation.DateTaskViewModel
import mx.com.disoftware.mydailytasks.presentation.DateTaskViewModelFactory
import mx.com.disoftware.mydailytasks.repository.DateTaskCrossRefRepositoryImpl
import mx.com.disoftware.mydailytasks.ui.task.adapter.ListTaskAdapter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

}