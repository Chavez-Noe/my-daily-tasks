package mx.com.disoftware.mydailytasks.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TaskEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_task")
    var idTask: Long = -1,
    @ColumnInfo(name = "title")
    var title: String = "title of task",
    @ColumnInfo(name = "description")
    var description: String = "description of task",
    @ColumnInfo(name = "status")
    var condition: String = Status.UNFINISHED.colorExa
) {
    constructor(title: String, description: String, condition: String) : this(
        0,
        title,
        description,
        condition
    )
}

enum class Status(val colorExa: String) {
    FINISHED("#68ff33"),   // green
    PARTIALLY("#ffe633"),  // yellow
    UNFINISHED("#ff3333")  // red
}