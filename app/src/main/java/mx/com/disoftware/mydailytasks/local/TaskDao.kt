package mx.com.disoftware.mydailytasks.local

import androidx.room.*
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

@Dao
interface TaskDao {

     @Query("SELECT * FROM taskentity")
     suspend fun getAllTasks(): List<TaskEntity>

     @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun saveTask(task: TaskEntity)

     @Delete
     suspend fun deleteTask(task: TaskEntity)

     @Query("SELECT * FROM taskentity WHERE title LIKE '%' || :nameTask || '%'")
     suspend fun findTaskByName(nameTask: String): List<TaskEntity>

     @Update
     suspend fun updateTask(Task: TaskEntity)

}