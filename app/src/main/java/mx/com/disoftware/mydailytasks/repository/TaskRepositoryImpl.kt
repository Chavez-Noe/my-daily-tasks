package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.local.LocalTaskDataSource
import mx.com.disoftware.mydailytasks.data.local.TaskEntity

class TaskRepositoryImpl(private val dataSourceLocal: LocalTaskDataSource) : TaskRepository {

    override suspend fun getTasks() = dataSourceLocal.getAllTask()

    override suspend fun save(task: TaskEntity) = dataSourceLocal.saveTask(task)

    override suspend fun findTaskByName(nameTask: String) = dataSourceLocal.findTaskByName(nameTask)

    override suspend fun deleteTask(task: TaskEntity) = dataSourceLocal.deleteTask(task)

    override suspend fun updateTask(task: TaskEntity) = dataSourceLocal.updateTask(task)

}