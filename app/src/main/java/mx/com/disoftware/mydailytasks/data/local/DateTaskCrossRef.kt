package mx.com.disoftware.mydailytasks.data.local

import androidx.room.Entity

@Entity(primaryKeys = ["idDate", "idTask"])
data class DateTaskCrossRef (
    var idDate: Long,
    var idTask: Long
)