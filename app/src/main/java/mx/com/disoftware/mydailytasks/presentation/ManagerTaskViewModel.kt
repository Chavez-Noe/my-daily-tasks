package mx.com.disoftware.mydailytasks.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.domain.ManageTasks

class ManagerTaskViewModel(
    private val manageTasks: ManageTasks
) : ViewModel() {
    fun registerNewTask(task: TaskEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(manageTasks.registerNewTask(task)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }
}

class ManagerTaskViewModelFactory(private val manageTasks: ManageTasks) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(ManageTasks::class.java).newInstance(manageTasks)
    }

}