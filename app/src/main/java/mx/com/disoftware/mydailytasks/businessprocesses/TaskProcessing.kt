package mx.com.disoftware.mydailytasks.businessprocesses

import kotlinx.coroutines.runBlocking
import mx.com.disoftware.mydailytasks.core.SystemDate
import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.data.local.TaskEntity
import mx.com.disoftware.mydailytasks.repository.DateRepository

class TaskProcessing(private val dateRepo: DateRepository) {

    private var dateActual: String = ""

    init {
        dateActual = SystemDate.getDateSystem()
    }
    
    public fun saveUnfinishedTask() {
        var id = 0L
        if (checkIfDateExists().isEmpty()) {
            runBlocking {
                dateRepo.saveDate(DateEntity(id++, dateActual))
            }
        }

    }

    public fun checkIfDateExists(): List<DateEntity> {
        val exist: List<DateEntity>
        runBlocking {
            exist = dateRepo.findByDate(dateActual)
        }
        return exist
    }

}