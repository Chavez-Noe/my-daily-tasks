package mx.com.disoftware.mydailytasks.local

import mx.com.disoftware.mydailytasks.data.local.TaskEntity

class LocalTaskDataSource(private val taskDao: TaskDao) {

    suspend fun getAllTask() = taskDao.getAllTasks()

    suspend fun saveTask(task: TaskEntity) {
        taskDao.saveTask(task)
    }

    suspend fun deleteTask(task: TaskEntity) {
        taskDao.deleteTask(task)
    }

    suspend fun findTaskByName(nameTask: String) = taskDao.findTaskByName(nameTask)

    suspend fun updateTask(task: TaskEntity) {
        taskDao.updateTask(task)
    }

}