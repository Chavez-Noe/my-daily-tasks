package mx.com.disoftware.mydailytasks.local

import android.util.Log
import mx.com.disoftware.mydailytasks.data.local.DateEntity

class LocalDateDataSource(private val dateDao: DateDao) {

    suspend fun saveDate(date: DateEntity) {
        dateDao.saveDate(date)
    }

    suspend fun deleteDate(date: DateEntity) {
        dateDao.deleteDate(date)
    }

    suspend fun updateDate(date: DateEntity) {
        dateDao.updateDate(date)
    }

    suspend fun getAllDate() = dateDao.getAllDate()

    suspend fun findByDate(nameDate: String) = dateDao.findByDate(nameDate)

}