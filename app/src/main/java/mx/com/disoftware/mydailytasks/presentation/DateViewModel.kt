package mx.com.disoftware.mydailytasks.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import mx.com.disoftware.mydailytasks.core.Result
import mx.com.disoftware.mydailytasks.data.local.DateEntity
import mx.com.disoftware.mydailytasks.repository.DateRepository

class DateViewModel(private val repo: DateRepository) : ViewModel() {

    fun fetchDate() = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.getDates()))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun deleteDate(dateEntity: DateEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.deleteDate(dateEntity)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun updateDate(dateEntity: DateEntity) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.updateDate(dateEntity)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun getDates() = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.getDates()))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

    fun findByDate(date: String) = liveData(Dispatchers.IO) {
        emit(Result.Loading())
        try {
            emit(Result.Success(repo.findByDate(date)))
        } catch (e: Exception) {
            emit(Result.Failure(e))
        }
    }

}

class DateViewModelFactory(private val repo: DateRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(DateRepository::class.java).newInstance(repo)
    }

}