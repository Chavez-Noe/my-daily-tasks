package mx.com.disoftware.mydailytasks.repository

import mx.com.disoftware.mydailytasks.data.local.DateEntity

interface DateRepository {

    suspend fun saveDate(date: DateEntity)

    suspend fun deleteDate(date: DateEntity)

    suspend fun updateDate(date: DateEntity)

    suspend fun getDates(): List<DateEntity>

    suspend fun findByDate(nameDate: String): List<DateEntity>

}