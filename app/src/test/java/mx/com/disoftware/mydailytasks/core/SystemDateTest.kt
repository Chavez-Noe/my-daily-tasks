package mx.com.disoftware.mydailytasks.core

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Month
import java.time.format.DateTimeFormatter
import java.util.*


class SystemDateTest {

    @Test
    fun testGetDateSystem() {
        val expected = SimpleDateFormat("dd-MM-yyyy").format(Date())
        val actual = SystemDate.getDateSystem()
        assertEquals(expected, actual)
    }

    @Test
    fun testGetCurrentTime() {
        val expected = SimpleDateFormat("HH:mm").format(Date())
        val actual = SystemDate.getCurrentTime()
        assertEquals(expected, actual)
    }

    @Test
    fun testGetPreviousDate() {
        val actual = SimpleDateFormat("dd-MM-yyyy").format(Date())
        val expected = SystemDate.getPreviousDate()
        assertNotEquals(expected, actual)
    }

    @Test
    fun testGetPreviousDateLimitMonth() {
        val actual = LocalDate.of(2021, Month.OCTOBER, 1)
        val format = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        actual.format(format)
        val expected = SystemDate.getPreviousDate()
        assertEquals(expected, actual)
    }

}